/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.labsoftdev4;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayers();
                isFinish = true;
            }
            else if(table.checkDraw()){
                printTable();
                printDraw();
                printPlayers();
                isFinish=true;
            }
            table.switchPlayer();
        }

    }

    private void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.err.print(t[i][j] + " ");
            }
            System.err.println("");
        }
    }

    private void printTurn() {
        System.err.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.err.println("Please input row col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void newGame() {
        table = new Table(player1,player2);
    }

    private void printWinner() {
        System.err.println(table.getCurrentPlayer().getSymbol() + " Win!!");
    }

    private void printDraw() {
        System.err.println("Draw");
    }
    
    private void printPlayers() {
        System.err.println(player1);
        System.err.println(player2);
    }

}
